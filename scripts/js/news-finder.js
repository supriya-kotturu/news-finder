class NewsFinder {
    constructor() {
        this.api_key = "addadb6a-061b-4ea0-a93e-14a9a36c85f7";
    }

    async getNewsResponse(userInput){
        
        const newsResponse = await fetch(`https://content.guardianapis.com/search?q=${userInput}&api-key=${this.api_key}&format=json&page-size=30&show-fields=trailText,headline,shortUrl,thumbnail&show-section=true`);
        
        // show-fields=headline,thumbnail,publication
        const newsRespData = await newsResponse.json();
        return newsRespData;
    }
}
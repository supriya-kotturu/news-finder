 $(document).ready(function(){
    $('input.autocomplete').autocomplete({
      data:{
            "Apple": 'static/Apple.png',
            "Microsoft": 'static/microsoft.png',
            "Google": 'static/google.png',
            "World": 'static/world.jpg',
            "COVID-19": 'static/corona.jpg',
            "Corona": 'static/corona.jpg',
            "Olympics": 'static/olympics.jpg',
            "Health": 'static/health.jpg',
            "Beauty": 'static/make-up.jpg',
            "Music": 'static/music.jpg',
            "Movies": null,
            "Films": null,
            "Latest": null,
            "News": null,
            "Politics": null,
            "Fitness": null,
            "Sports": null
        },
    });
  });

const newsFinder = new NewsFinder();
const ui = new UI();

// getting the input element
const searchNews = document.getElementById("user-input");

// adding the event listner to the input field
searchNews.addEventListener("keyup", (e) => {
    let userIP = e.target.value;
    if (userIP !== "") {
        newsFinder.getNewsResponse(userIP)
            .then((data) => {
                if (data.response.results.length > 0) {
                    // console.log(data); // .response.results
                    ui.showNews(data.response.results);
                } else {
                    // console.log("not found");
                    ui.showAlert("Cannot be found", "alert");
                }
            });
    } else {
        ui.clearResults();
    }
});
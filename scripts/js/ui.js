class UI {
    constructor() {
        this.news = document.getElementById("results");
    }

    showNews(news) {
        this.news.innerHTML = " ";
        // console.log(news);
        news.forEach(newsItem => {
            this.news.innerHTML += `<div class="col s12 l4 m6">
            <div class="card large">
              <div class="card-image">
                <img src=${newsItem.fields.thumbnail}>
              </div>
              <div class="card-content">
              <h6>${newsItem.fields.headline}</h6>
                <p class="thin">${newsItem.fields.trailText}</p>
              </div>
              <div class="card-action">
              <a class="waves-effect waves-light btn" target="blank" href=${newsItem.fields.shortUrl}>Read more</a><span offset-m3>
              <a href='#'>&nbsp ${newsItem.sectionId}<span class="material-icons">
              local_offer
              </span></a></span>
              </div>
            </div>
          </div>`
        });
    }

    showAlert(message, className) {
        this.clearAlert();
        const div = document.createElement("div");
        div.className = className;
        div.innerHTML = `<div class="col s10 m6 offset-m3 offset-s1 l6 offset-l3">
        <div class="card light-tulip ">
          <div class="card-content white-text">
            <span class="card-title">${message}</span>
          </div>
        </div>` ;
        //get parent node and the node before you want to instert
        const container = document.getElementById("search-results");
        const results = document.getElementById("results");
        container.insertBefore(div, results);

        setTimeout(()=> {this.clearAlert()} ,2500);
    }

    clearAlert() {
        const currentAlert = document.querySelector('.alert');
        if(currentAlert){
            currentAlert.remove();
        }
    }

    clearResults() {
        this.news.innerHTML = " ";
    }
}
